<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="models.Holder" %>

<!DOCTYPE html>
<html>
    <body class="body">
    <h1>Holders</h1>
    <table border:1 rules="rows">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>show</th>
            <th>serialize</th>
        </tr>
        <%for (Holder holder : Holder.all()) {%>
        <tr>
            <td><%= holder.getId() %>
            </td>
            <td><%= holder.getName() %>
            </td>
            <td><%= holder.getPhone() %>
            </td>
            <td>
                <form method="get" action="/Tomcat2/holder_show_serialized/<%= holder.getId() %>">
                    <button class="button" type="submit">show</button>
                </form>
            </td>
            <td>
                <form method="post" action="/Tomcat2/holder_serialize/<%= holder.getId() %>">
                    <button class="button" type="submit">serialize</button>
                </form>
            </td>
        </tr>
        <%}%>
    </table>
    </body>
</html>
