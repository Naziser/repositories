<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="models.Holder" %>

<!DOCTYPE html>
<html>
    <body class="body">
        <h1>Holder</h1>
        <p><%=((Holder) request.getAttribute("holder")).toString()%></p>
        <form method="get" action="/Tomcat2/holders_serialization">
            <button class="button" type="submit">exit</button>
        </form>
    </body>
</html>
