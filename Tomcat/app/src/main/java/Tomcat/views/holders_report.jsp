<%@ page import="dto.Holder" %>
<%@ page import="dto.Equipment" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%String phone = request.getParameter("phone");%>

<!DOCTYPE html>
<html>
<body class="body">
    <form method="get">
        <label for="phone"> Phone:</label>
        <input class="input" id="phone" type="text" name="phone" value="<%= phone %>" >
        <button class="button" type="submit">found</button>
    </form>
    <%Holder holder = (Holder) request.getAttribute("Holder");%>
    <h2>id: <%=holder.getId()%></h2>
    <h2>name: <%=holder.getName()%></h2>
    <h2>phone: <%=holder.getPhone()%></h2>
    <table>
        <thead>
            <tr>
                <td>num</td>
                <td>Equipment</td>
            </tr>
        </thead>
        <tbody>
            <%int i = 1;%>
            <%for (Equipment equipment : (ArrayList<Equipment>) request.getAttribute("Equipment")){%>
            <tr>
                <td><%=i%></td>
                <td><%= equipment.getEquipment()%><td>
            </tr>
            <%i++;%>
            <%}%>
        </tbody>
    </table>
</body>
</html>
