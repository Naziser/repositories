<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="models.Holder" %>
<%Holder holder = (Holder)request.getAttribute("holder"); %>
<body>
    <h1 style="font-size:30px" align="center"><%= request.getAttribute("text")%></h1>
        <% if (request.getAttribute("holder") != null) { %>
            <p style="font-size:20px" align="center">
                <%= holder.getName()%>
                <%= holder.getPhone()%>
            </p>
        <%}%>
</body>
