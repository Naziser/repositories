<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="models.Holder" %>
<%Holder[] holders = (Holder[]) request.getAttribute("holders"); %>
<body>
    <h1 style="font-size:30px" align="center"><%= request.getAttribute("text")%></h1>
    <% if (holders != null) { %>
        <%for (Holder h : holders){%>
            <p style="font-size:20px" align="center">
                <%= h.getName()%>
                <%= h.getPhone()%>
            </p>
        <%}%>
    <%}%>
</body>
