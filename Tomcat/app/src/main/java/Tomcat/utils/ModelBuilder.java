package utils;

import java.sql.ResultSet;

public interface ModelBuilder<T> {
     T build(ResultSet resultSet);
}
