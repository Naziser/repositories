package utils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class GetFromeRequest {

    public static int getId(HttpServletRequest request) {
        Matcher matcher = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURI());
        return (matcher.find()) ? Integer.parseInt(matcher.group()) : -1;
    }
}
