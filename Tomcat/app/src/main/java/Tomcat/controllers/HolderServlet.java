package controllers;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.sql.*;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.io.IOException;
import java.io.PrintWriter;
import org.json.JSONObject;

import javax.servlet.ServletException;

import models.Holder;

@WebServlet("/holders/*")
public class HolderServlet extends HttpServlet
{
    private static String message = "Error during Servlet processing";

    private String getPhone(HttpServletRequest request) {
      Matcher matcher = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURI());
      matcher.find();
      return matcher.group();
    }

    private void htmlOutput(HttpServletRequest request, HttpServletResponse response,
        Holder holder, String text) throws IOException, SQLException, ServletException {
        request.setAttribute("text", text);
        if (holder != null) {
            request.setAttribute("holder", holder);
        }
        request.getRequestDispatcher("/holder.jsp").forward(request, response);
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
           response.setContentType("text/html; charset=UTF-8");
           response.setCharacterEncoding("UTF-8");
           request.setCharacterEncoding("UTF-8");
           PrintWriter out = response.getWriter();
           try{
              Holder holder = Holder.getbyPhone(getPhone(request));
              if (holder != null) {
                  htmlOutput(request, response, holder, "Holder");
              }
              else {
                  htmlOutput(request, response, null, "Holder not found");
              }
           } catch (Exception ex) {
               ex.printStackTrace();
           }
    }


    public void doPut(HttpServletRequest request, HttpServletResponse response)
                            throws IOException, ServletException {
        System.out.println("Enter doPut");
        request.setCharacterEncoding("UTF-8");
        try {
            String numberPhone = getPhone(request);
            String body = request.getReader().lines().reduce("", (accumulator, actual) -> accumulator + actual);
            System.out.println(body);
            JSONObject data = new JSONObject(body);
            String name1 = data.getString("name");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + "/holders/" + getPhone(request));
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response)
                            throws IOException, ServletException {
        try{
            String numberPhone = getPhone(request);
            Holder.deletebyPhone(numberPhone);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + "/holders");
    }

}
