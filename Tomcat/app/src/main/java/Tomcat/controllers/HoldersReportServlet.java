package controllers;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

import query_object.QueryObject;
import dto.HolderWrapper;
import dto.Holder;
import dto.Equipment;


/**
  * Address servlet class "/holders_report"
  * @author Naziser
  * @version %I%, %G%
  */
@WebServlet("/holders_report")
public class HoldersReportServlet extends HttpServlet
{

    /**
     * method of processing GET requests, output of the holders_report page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                                    throws ServletException, IOException {
        ArrayList<HolderWrapper> wrappers = QueryObject.getHolderQuery(request.getParameter("phone"));
        Holder holder = new Holder(wrappers.get(0));
        ArrayList<Equipment> equipments = Equipment.getEquipments(wrappers);
        System.out.println(wrappers);
        request.setAttribute("Holder", holder);
        request.setAttribute("Equipment", equipments);
        request.getRequestDispatcher("/holders_report.jsp").forward(request, response);
    }
}
