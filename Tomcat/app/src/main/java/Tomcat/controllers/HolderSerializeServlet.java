package controllers;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;

import models.Holder;
import service.Serialize;
import models.SerializedHolder;
import utils.GetFromeRequest;

@WebServlet("/holder_serialize/*")
public class HolderSerializeServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws  IOException, ServletException{
        int holderId = GetFromeRequest.getId(request);
        System.out.println(holderId);
        Holder holder = Holder.findById(holderId);
        System.out.println(Holder.findById(holderId));
        if (holder != null) {
            SerializedHolder.create(Serialize.serialize((Serializable) holder), holder.getId());
            String outMessage = "Serialization was successful";
            createOutHTML(response, request, outMessage);
        } else {response.sendError(HttpServletResponse.SC_NOT_FOUND);}
    }

    private static void createOutHTML(HttpServletResponse response, HttpServletRequest request, String message) throws IOException, ServletException {
        request.setAttribute("message", message);
        request.getRequestDispatcher("/holder_serialize.jsp").forward(request, response);
    }
}
