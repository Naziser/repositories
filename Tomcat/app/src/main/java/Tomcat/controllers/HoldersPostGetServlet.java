package controllers;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.sql.*;
import java.util.ArrayList;

import models.Holder;

@WebServlet("/holders")
public class HoldersPostGetServlet extends HttpServlet
{
    private static String message = "Error during Servlet processing";

    private void htmlOutput(HttpServletRequest request, HttpServletResponse response,
        ArrayList<Holder> holders, String text) throws IOException, SQLException, ServletException {
        request.setAttribute("text", text);
        request.setAttribute("holders", holders.toArray(new Holder[0]));

        request.getRequestDispatcher("/holders.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        System.out.println("Enter doPost");
        try {
            request.setCharacterEncoding("UTF-8");
            String phone = request.getParameter("phone");
            String name = request.getParameter("name");
            if (name == null || phone == null || Holder.getbyPhone(phone) != null) {
                response.sendError(422);
            } else if (Holder.create(phone, name)) {
                ArrayList<Holder> h = new ArrayList<Holder>();
                h.add(Holder.getbyPhone(phone));
                htmlOutput(request, response, h, "Holder create");
            } else {
                htmlOutput(request, response, null, "Holder not create");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
        try {
            htmlOutput(request, response, Holder.getAll(), "Holders");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
