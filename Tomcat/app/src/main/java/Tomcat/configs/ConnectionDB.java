package configs;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The ConnectionDB class is used to create a database connection
 * @author Naziser
 */
public class ConnectionDB {

    /** @value field of the Connection object */
    private static Connection connection;

    /**
     * createConnection method
     * creates a database connection
     * @param url - database url
     * @param user - name of the database
     * @param password - Connection password
     * @return field of the Connection object
     */
    public static Connection createConnection(String url, String user, String password) throws SQLException{
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * createConnection method
     * checks the status of the connection field
     * @return field of the Connection object
     */
    public static Connection getConnection() throws SQLException{
        if (connection == null){
            connection = createConnection("jdbc:postgresql://127.0.0.1:999/Tomcat", "postgres", "123");
        }
        return connection;
    }
}
