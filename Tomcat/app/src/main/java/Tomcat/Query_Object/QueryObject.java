package query_object;
import java.sql.SQLException;
import java.util.ArrayList;

import utils.BaseConnection;
import dto.HolderWrapper;
import utils.ModelBuilder;

/**
  * This is QueryObject.
  * @author Naziser
  * @version %I%, %G%
  */
public class QueryObject extends BaseConnection{

    /**
     * Мы хотим получить список пользователей с их id, экиперовкой и номером телефона по номеру телефона.
     * sql query that allows you to get
     * holder_id
     * name
     * phone
     * title
     * by phone
     */
    public static String holderQuery = "select holder.id as holder_id, holder.name, holder.phone, equipment.title\n" +
    "from holder\n" +
    "join equipment_to_holder on equipment_to_holder.holder_id = holder.id\n" +
    "join equipment on equipment_to_holder.equipment_id = equipment.id\n" +
    "where holder.phone = '%s';";


    /**
     * Method to get ArrayList<HolderWrapper>
     * @param phone phone.
     * @return ArrayList<HolderWrapper>.
     */
    public static ArrayList<HolderWrapper> getHolderQuery(String phone) {
        System.out.println(String.format(holderQuery, phone));
        return executeSelect(String.format(holderQuery, phone),buildModel);
    }

    /**
     * lambda result
     * creates and populates a HolderWrapper instance
     */
    public static final ModelBuilder<HolderWrapper> buildModel = (resultSet) -> {
        HolderWrapper holderWrapper = new HolderWrapper();
        try {
            holderWrapper.setId(resultSet.getInt("holder_id"));
            holderWrapper.setName(resultSet.getString("name"));
            holderWrapper.setPhone(resultSet.getString("phone"));
            holderWrapper.setEquipmentTitle(resultSet.getString("title"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return holderWrapper;
    };
}
