package dto;

/**
  * This is HolderWrapper.
  * @author Naziser
  * @version %I%, %G%
  */
public class HolderWrapper{

    /** @value field to store name holder */
    private String name;

    /** @value field to store id holder */
    private int id;

    /** @value field to store phone holder */
    private String phone;

    /** @value field to store equipmentTitle holder */
    private String equipmentTitle;

    /**
     * HolderWrapper constructor
     * @param id - id holder
     * @param name - name holder
     * @param phone - phone holder
     * @param equipmentTitle - equipmentTitle holder
     */
    public HolderWrapper(int id, String name, String phone, String equipmentTitle) {
        this.name = name;
        this.id = id;
        this.phone = phone;
        this.equipmentTitle = equipmentTitle;
    }

    /** Empty HolderWrapper constructor */
    public HolderWrapper(){}

    /**
     * method getId
     * @return holder id
     */
    public int getId() {
        return id;
    }

    /**
     * method setID
     * @param id - holder id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getName
     * @return holder name
     */
    public String getName() {
        return name;
    }

    /**
     * method setName
     * @param name - holder name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method getPhone
     * @return holder phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * method setPhone
     * @param phone - holder phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * method getEquipmentTitle
     * @return holder id
     */
    public String getEquipmentTitle() {
        return equipmentTitle;
    }

    /**
     * method setEquipmentTitle
     * @param equipmentTitle - holder equipmentTitle
     */
    public void setEquipmentTitle(String equipmentTitle) {
        this.equipmentTitle = equipmentTitle;
    }

    /**
     * method toString
     * @return String name phone - equipmentTitle
     */
    public String toString() {
        return String.format("%s  %s - %s", name, phone, equipmentTitle);
    }
}
