package dto;

import java.util.ArrayList;

/**
  * This is HolderWrapper.
  * @author Naziser
  * @version %I%, %G%
  */
public class Equipment {

    private String equipmentTitle;

    /**
     * HolderWrapper constructor
     * @param HolderWrapper - Holder wrapper
     */
    public Equipment(HolderWrapper holderWrapper){
        this.equipmentTitle = holderWrapper.getEquipmentTitle();
    }

     /**
     * method getEquipment
     * @return equipment title
     */
    public String getEquipment(){
        return this.equipmentTitle;
    }

    /**
     * method getEquipments
     * @return equipments
     */
    public static ArrayList<Equipment> getEquipments(ArrayList<HolderWrapper> holdersWrapper){
        ArrayList<Equipment> equipments = new ArrayList<>();
        for (HolderWrapper holderWrapper : holdersWrapper){
            equipments.add(new Equipment(holderWrapper));
        }
        return equipments;
    }
}
