package dto;

/**
  * This is HolderWrapper.
  * @author Naziser
  * @version %I%, %G%
  */
public class Holder {
    /** @value field to store name holder */
    private String name;

    /** @value field to store id holder */
    private int id;

    /** @value field to store phone holder */
    private String phone;

    /**
     * Holder constructor
     * @param HolderWrapper - Holder wrapper
     */
    public Holder(HolderWrapper HolderWrapper) {
        this.name = HolderWrapper.getName();
        this.id = HolderWrapper.getId();
        this.phone = HolderWrapper.getPhone();
    }

    /**
     * method getId
     * @return holder id
     */
    public int getId() {
        return id;
    }

    /**
     * method setID
     * @param id - holder id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getName
     * @return holder name
     */
    public String getName() {
        return name;
    }

    /**
     * method setName
     * @param name - holder name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * method getPhone
     * @return holder phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * method setPhone
     * @param phone - holder phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * method toString
     * @return String name phone
     */
    public String toString() {
        return String.format("%s  %s", name, phone);
    }
}
