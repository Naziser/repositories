package models;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.io.ByteArrayInputStream;

import utils.BaseConnection;
import utils.ModelBuilder;

public class SerializedHolder extends BaseConnection {

    private String data;

    private int holderId;

    private static final ModelBuilder<SerializedHolder> modelExtractor = (rs) -> {
        SerializedHolder serializedHolder = null;
        try {
            serializedHolder = new SerializedHolder(
                    rs.getString("data"),
                    rs.getInt("holder_id")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return serializedHolder;
    };

    private static final String[] COLUMN_NAMES_INSERT = new String[]{"data", "holder_id"};

    private static final String[] COLUMN_NAMES_SELECT = new String[]{"id", "data", "holder_id"};

    private static final String TABLE_NAME = "serialized_holder";

    public SerializedHolder(String data, int holderId) {
        this.data = data;
        this.holderId = holderId;
    }

    public String getData() {
        return data;
    }

    public int getHolderId() {
        return holderId;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static int create(String data, int holderId) {
        PreparedStatement stmtCreate = getPreparedStatement(
                String.format("INSERT INTO %s (%s) VALUES(?, ?)", TABLE_NAME, String.join(", ", COLUMN_NAMES_INSERT)),
                new String[]{"holder_id"}
        );
        int newId = -1;
        try {
            stmtCreate.setString(1, data);
            stmtCreate.setInt(2, holderId);
            stmtCreate.executeUpdate();
            ResultSet gk = stmtCreate.getGeneratedKeys();
            if (gk.next()) {
                newId = gk.getInt("holder_id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return newId;
    }

    public static SerializedHolder findByHolderId(int holderId) {
        return executeSelectOneInstance(String.format("SELECT %s FROM %s WHERE holder_id = %s ORDER BY %s DESC LIMIT 1", String.join(", ", COLUMN_NAMES_SELECT), TABLE_NAME, holderId, COLUMN_NAMES_SELECT[0]));
    }

    private static SerializedHolder executeSelectOneInstance(String query) {
        SerializedHolder[] serializedHolderInList = executeSelect(query);
        return (serializedHolderInList.length > 0) ? serializedHolderInList[0] : null;
    }

    private static SerializedHolder[] executeSelect(String query) {
        ArrayList<SerializedHolder> serializedHolders = executeSelect(query, modelExtractor);
        return serializedHolders.toArray(new SerializedHolder[serializedHolders.size()]);
    }
}
