package models;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.*;
import java.io.Serializable;

import utils.ModelBuilder;
import utils.BaseConnection;

public class Holder extends BaseConnection implements Serializable {

    private int id;
    private String name;
    private String phone;
    private static final String[] COLUMN_NAMES = {"name", "phone"};
    private static final String TABLE_NAME = "Holder";

    Holder(String name, String phone, int id) {
       this.name = name;
       this.phone = phone;
       this.id = id;
   }

   Holder(){}

    private static ModelBuilder<Holder> buildModel = (rs) ->
    {
      Holder holder = null;
      try {
        holder = new Holder(rs.getString("name"), rs.getString("phone"));
      } catch (SQLException e) {
          System.out.println(e.getMessage());
      }
      return holder;
    };


    public Holder(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public static ArrayList<Holder> executeWithResultSet(String query) throws SQLException{
        return executeSelect(query, buildModel);
    }

    public static boolean executeWithoutResultSet(String query, String phone) throws SQLException{
        executeWithoutReturn(query);
        return getbyPhone(phone) != null;
    }

// new
    private static Holder executeSelectOneInstance(String query) {
        Holder[] holderInList = executeSelect(query);
        return (holderInList.length > 0) ? holderInList[0] : null;
    }

// new
    private static Holder[] executeSelect(String query) {
        ArrayList<Holder> holders = executeSelect(query, buildModel);
        return holders.toArray(new Holder[holders.size()]);
    }




    public static ArrayList<Holder> getAll() throws SQLException{
        return executeWithResultSet(String.format("SELECT %s FROM Holder", String.join(", ", COLUMN_NAMES)));
    }

// new
    public static Holder[] all() {
        return executeSelect("SELECT * FROM " + TABLE_NAME);
    }


    public static Holder getbyPhone(String phone) throws SQLException{
        ArrayList<Holder> hol = executeWithResultSet(String.format("SELECT %s FROM Holder WHERE phone in ('%s')",
                                                                            String.join(", ", COLUMN_NAMES), phone));
        return hol.isEmpty() ? null : hol.get(0);
    }

// new
    public static Holder findByPhone(String phone) {
       return executeSelectOneInstance(String.format("SELECT %s, id FROM %s WHERE phone = '%s'", String.join(", ", COLUMN_NAMES), TABLE_NAME, phone));
   }

 // new
    public static Holder findById(int id) {
       return executeSelectOneInstance(String.format("SELECT %s, id FROM %s WHERE id = %s", String.join(", ", COLUMN_NAMES), TABLE_NAME, id));
   }



    public static boolean create(String phone, String value) throws SQLException{
        return executeWithoutResultSet(String.format("INSERT INTO Holder (%s) VALUES('%s', '%s')",
                                                        String.join(", ", COLUMN_NAMES), value, phone), phone);
    }

// new
    public static int createint(String name, String phone) {
       PreparedStatement stmtCreate = getPreparedStatement(
               String.format("INSERT INTO %s (%s) VALUES(?, ?)", TABLE_NAME, String.join(", ", COLUMN_NAMES)),
               new String[]{"id"}
       );
       int newId = -1;
       try {
           stmtCreate.setString(1, name);
           stmtCreate.setString(2, phone);
           stmtCreate.executeUpdate();
           ResultSet gk = stmtCreate.getGeneratedKeys();
           if (gk.next()) {
               newId = gk.getInt("id");
           }
       } catch (SQLException e) {
           System.out.println(e.getMessage());
       }
       return newId;
   }

// new
   public boolean save() {
        Holder holder = findById(id);
        if (holder != null) {
            PreparedStatement stmtUpdate = getPreparedStatement(
                    String.format("UPDATE %s SET %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", COLUMN_NAMES)));
            try {
                stmtUpdate.clearParameters();
                stmtUpdate.setString(1, name);
                stmtUpdate.setString(2, phone);
                stmtUpdate.setInt(3, id);
                stmtUpdate.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } else {
            id = createint(name, phone);
            holder = findById(id);
        }
        return holder != null && holder.getName().equals(name) && holder.getPhone().equals(phone);
    }




    public static boolean deletebyPhone(String phone) throws SQLException{
        return !executeWithoutResultSet(String.format("DELETE FROM Holder WHERE %s in ('%s')",
                                                                  COLUMN_NAMES[1], phone), phone);
    }

    public boolean delete() {
       executeWithOutRs(String.format("DELETE FROM %s WHERE id = %s", TABLE_NAME, id));
       return findById(id) == null;
   }


    public static boolean update(String phone, String name) throws SQLException{
        return executeWithoutResultSet(String.format("UPDATE Holder SET %s = '%s' WHERE %s = '%s'",
                                                      COLUMN_NAMES[0], name, COLUMN_NAMES[1], phone), phone);
    }

// new
    @Override
    public String toString() {
        return String.format("id: %s; name: %s; phone %s", id, name, phone);
    }

}
