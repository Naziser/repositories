package SleepingBarber;

public class Customer implements Runnable {
    private CStatus status = CStatus.LOOKING_AT_THE_BARBER;
    private Barber barber;
    private Queue queue;
    private String name;
    Customer(Barber barber, Queue queue) {
        this.barber = barber;
        this.queue = queue;
    }
    public void setStatus(CStatus status) {
        this.status = status;
    }
    public String getName() {
        return name;
    }
    @Override
    public void run() {
        name = Thread.currentThread().getName();
        queue.closeCorridor();
        if (barber.getStatus().equals(BStatus.SLEEPING)) {
            System.out.format("Клиент %s нашел отдыхающего барбера и будит его\n", name);
            barber.toWake(this);
        } else {
            status = CStatus.LOOKING_IN_THE_HALL;
            System.out.format("Клиент %s увидел работающего барбера и смотрит в очередь\n", name);
            if (queue.sitOnAChair(this)) {
                System.out.format("Клиент %s сел на стул в очереди\n", name);
                queue.openCorridor();
            } else {
                System.out.format("Клиент %s не нашел места в очереди и уходит\n", name);
                queue.openCorridor();
            }
        }
    }
}
