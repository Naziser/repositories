package SleepingBarber;

import static SleepingBarber.Sleep.sleep;
public class Barber implements Runnable {
    private static final int MIN_TIME_FOR_HAIRCUT = 3000;
    private static final int TIME_FOR_HAIRCUT_INTERVAL = 2000;
    private BStatus status = BStatus.LOOKING_IN_THE_HALL;
    private Queue queue;
    private Chair chair;
    Barber(Queue queue, Chair chair) {
        this.queue = queue;
        this.chair = chair;
    }
    public BStatus getStatus() {
        return status;
    }
    public synchronized void toWake(Customer customer) {
        System.out.println("Парикмахера разбудил клиент " + customer.getName());
        chair.vacate();
        status = BStatus.WORKING;
        queue.openCorridor();
        cutHair(customer);
        this.notify();
    }
    private void cutHair(Customer client) {
        String clientName = client.getName();
        System.out.println("Парикмахер начинает стричь клиента " + clientName);
        chair.occupy(client);
        client.setStatus(CStatus.GETTING_A_HAIRCUT);
        sleep(MIN_TIME_FOR_HAIRCUT, TIME_FOR_HAIRCUT_INTERVAL);
        System.out.println("Парикмахер достриг клиента " + clientName);
        chair.vacate();
        client.setStatus(CStatus.FREED);
    }
    @Override
    public void run() {
        Customer customer;
        while (true) {
            queue.closeCorridor();
            status = BStatus.LOOKING_IN_THE_HALL;
            System.out.println("Парикмахер исследует очередь");
            if (queue.getNumberOfClients() > 0) {
                status = BStatus.WORKING;
                customer = queue.getClient();
                queue.openCorridor();
                cutHair(customer);
            } else {
                System.out.println("Парикмахер отдыхает");
                status = BStatus.SLEEPING;
                chair.occupy();
                queue.openCorridor();
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }
}

