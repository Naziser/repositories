package DiningPhilosophers;


public class Main {

    public static void main(String[] args) {
        final int COUNT_PHILOSOPHER = 5;

        Waiter waiter = new Waiter(COUNT_PHILOSOPHER);
        Fork[] forks = new Fork[COUNT_PHILOSOPHER];
        Philosoph[] philosophers = new Philosoph[COUNT_PHILOSOPHER];

        for (int i = 0; i < COUNT_PHILOSOPHER; i++){
            forks[i] = new Fork();
        }

        for (int i = 0; i < COUNT_PHILOSOPHER - 1; i++){
            philosophers[i] = new Philosoph("Философ " + (i + 1), forks[i], forks[i + 1], waiter);
        }

        philosophers[COUNT_PHILOSOPHER - 1] = new Philosoph(
                "Философ " + COUNT_PHILOSOPHER , forks[COUNT_PHILOSOPHER - 1], forks[0], waiter);

        for (Philosoph philosopher: philosophers){
            new Thread(philosopher).start();
        }
    }

}
