package DiningPhilosophers;

import java.util.concurrent.Semaphore;

/**
 * класс, реализующий поведение официанта, обслуживающего философов
 */
public class Waiter {
    /**
     * количетсво вилок необходимых для трапезы
     */
    private static final int COUNT_FORK = 2;
    /**
     * Семафор для использования вилки
     */
    public Semaphore forks;
    /**
     * конструктор класса Philosopher
     * @param countFork количетсво вилок на столе
     */
    Waiter(int countFork) {
        this.forks = new Semaphore(countFork, true);
    }
    /**
     * метод , чтобы брать вилки со стола
     */
    public void accessToFork() {
        try {
            this.forks.acquire(COUNT_FORK);
        } catch (InterruptedException e) {}
    }
    /**
     * метод, чтобы ложить вилки на стол
     */
    public void giveFork() {
        this.forks.release(COUNT_FORK);
    }
}
