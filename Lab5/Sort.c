#include <stdint.h>
#include <stdio.h>

int main() {
  int array[5] = {2, 8, 3, 1, 6};
  int i = 0;
  int temp = 0;
  int n = 5;
  while (i != n) {
    if (array[i] > array[i+1]) {
      temp = array[i+1];
      array[i+1]=array[i];
      array[i] = temp;
      i = 0;
    }
    else {
      i = i + 1;
    }
  }
  for (i = 0; i < n; i++) {
  printf("%d", array[i]);
  }

}
