package Task7.Actions;

public class Action{
    String str;
    String poem;

    public Action(String str){
        this.str = str;
    }

    public void act(){
      System.out.println(this.poem);
    }

    public String getStr(){
        return this.str;
    }

    public void setPoem(String poem){
      this.poem = poem;
    }
}
