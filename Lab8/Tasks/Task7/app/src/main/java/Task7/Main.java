package Task7;
import java.util.Scanner;
import Task7.Actions.*;

class Main{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int userResponse = 0;
        Poems begin = new Poems();
        Poems level1 = new Poems();
        Poems level1_2 = new Poems();
        Poems level2 = new Poems();
        Poems level2_2 = new Poems();

        Action one = new Action("Вывести стих 'Гой ты ,Русь, моя родная'");
        one.setPoem("Гой ты, Русь, моя родная");
        Action two = new Action("Вывести стих 'Письмо матери'");
        two.setPoem("Письмо матери");
        Action three = new Action("Вывести стих 'Мне осталась одна забава'");
        three.setPoem("Мне осталась одна забава");
        Action four = new Action("Вывести стих 'Что это такое?'");
        four.setPoem("Что это такое?");
        Action five = new Action("Вывести стих 'Заметался пожар голубой'");
        five.setPoem("Заметался пожар голубой");

        begin.connection(level1);
        begin.connection(level1_2);
        begin.setAction(one);

        level1.connection(level2);
        level1.connection(level2_2);
        level1.setAction(two);

        level1_2.setAction(three);
        level2.setAction(four);
        level2_2.setAction(five);

        while(true){
            begin.printStr();
            userResponse = in.nextInt();
            System.out.println();
            if (userResponse == 0){
                if (begin.parent == null){
                    break;
                }
                else {
                    begin = begin.parent;
                }
            }

            if(userResponse > 0 && userResponse < begin.actions.size() + begin.children.size()){
                begin = begin.children.get(userResponse - begin.children.size() + 1);
            } else if(userResponse > begin.children.size() && userResponse <= begin.actions.size() + begin.children.size()){
                begin.actions.get(0).act();
            }
        }
    }
}
