class Sort {
  public static void main(String[] args) {
    int array[] = {2, 1, 4, 7, 6};
    int i=0;
    int temp = 0;
    while (i != (array.length - 1)) {
      if (array[i] > array[i+1]) {
        temp = array[i+1];
        array[i+1] = array[i];
        array[i] = temp;
        i = 0;
      }
      else {
        i++;
      }
    }
    for (i=0; i < array.length; i++) {
      System.out.println(array[i]);
    }
 }
}
