package Structure;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SingleLinkedList<T> implements List<T> {
    Node<T> head = null;
    Node<T> tail = null;

    /**
      * Возвращает размер списка
    */
    @Override
    public int size() {
        Node<T> node = this.head;
        int size = 0;
        while (node != null) {
            size++;
            node = node.getNext();
        }
        return size;
    }


    /**
      * Возвращает true , если список пустой , иначе возвращает false
      @see size()
    */
    @Override
    public boolean isEmpty() {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
      * Возвращает true , если список содержит объект
      @param Object o
      @return boolean
    */
    @Override
    public boolean contains(Object o) {
        Node<T> node = this.head;
        while (node != null) {
            if (node.getData() == o) {
                return true;
            }
            node = node.getNext();
        }
        return false;
    }


    /**
      * Возвращает итератор по элементам в этом списке в правильной последовательности.
    */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> current = head;

            public boolean hasNext() {
                if (current != null) {
                    return true;
                }
                return false;
            }

            public T next() {
                T data = (T) current.getData();
                current = current.getNext();
                return data;
            }
        };
    }


    /**
      * Возвращает массив, содержащий все элементы в этом списке в правильной последовательности (от первого до последнего элемента).
    */
    @Override
    public T[] toArray() {
        int size = this.size();
        T[] arr = (T[]) new Object[size];
        Node<T> node = this.head;
        for (int i = 0; i < size; i++) {
            arr[i] = (T) node.getData();
            node = node.getNext();
        }
        return arr;
    }


    /**
      * Добавляет указанный элемент в конец этого списка
    */
    @Override
    public boolean add(T data) {
        Node<T> node = new Node<T>((T) data);
          if (this.head == null) {
            this.head = node;
            this.tail = node;
          } else {
            this.tail.setNext(node);
            this.tail = node;
          }
          return true;
    }


    /**
      * Удаляет первое вхождение указанного элемента из этого списка, если он присутствует (необязательная операция).
    */
    @Override
    public boolean remove(Object o) {
        Node<T> node = this.head;
        Node<T> next = new Node<T>();
        while (node != null) {
          if (node.getNext().getData() == o) {
            next = node.getNext();
            node.setNext(next.getNext());
            next.setData(null);
            next.setNext(null);
            return true;
          }
        }
        return false;
    }


    /**
      * Добавляет все элементы указанной коллекции в конец этого списка в том порядке, в котором они возвращаются итератором указанной коллекции
    */
    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        Node<T> next = new Node<T>();
        while (this.head != null) {
            next = this.head.getNext();
            this.head.setData(null);
            this.head.setNext(null);
            this.head = next;
        }
    }

    @Override
    public T get(int index) {
        if(index < 0 || index > this.size()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return (T) this.head;
        }
        Node<T> node = this.head;
        while (index != 0) {
            index--;
            node = node.getNext();
        }
        return (T) node;
    }

    @Override
    public T set(int index, Object element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray(Object[] a) {
        int size = this.size();
        a = new Object[size];
        Node<T> node = this.head;
        for (int i = 0; i < size; i++) {
            a[i] = node.getData();
            node = node.getNext();
        }
        return a;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof SingleLinkedList)) {
            System.out.println("Other class");
            return false;
        }
        SingleLinkedList<T> newList = (SingleLinkedList<T>) o;
        if (this.size() != newList.size()) {
            System.out.println("Other size");
            return false;
        }
        Node<T> node = this.head;
        Node<T> newNode = newList.head;
        while (node != null) {
            if (node.getData() != newNode.getData()) {
                return false;
            }
            node = node.getNext();
            newNode = newNode.getNext();
        }
        return true;
    }
}
