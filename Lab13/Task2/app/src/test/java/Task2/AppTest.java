/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void First(){
        App text = new App();
        assertEquals(text.getWord("В лотерее победил пятый номер."),"номер ");
    }

    @Test void Third(){
        App text = new App();
        assertEquals(text.getWord("Пятая колонна пришла в гости"),"колонна ");
    }

    @Test void Fourth(){
        App text = new App();
        assertEquals(text.getWord("пятый элемент залетел в окно"),"элемент ");
    }

    @Test void Fifth(){
        App text = new App();
        assertEquals(text.getWord("Шел пятый час... нужно бы бежать, да только пятая точка прилипла к стулу"),"час точка ");
    }

    @Test void  Sixth(){
        App text = new App();
        assertEquals(text.getWord("Шелпятый час... нужно бы бежать, да только пятая точка прилипла к стулу"),"точка ");
    }

    @Test void Seventh(){
        App text = new App();
        assertEquals(text.getWord("...пятый час... нужно бы бежать, да только пятая точка прилипла к стулу"),"час точка ");
    }
}
