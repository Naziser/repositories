package Task1;

public class Matrix {
    private int[][] matrix;
    private int n;
    private int m;

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        this.matrix = new int[this.n][this.m];
        generateMatrix();
    }

    public Matrix() {
    }

    public void setN(int n) {
        this.n = n;
        this.matrix = new int[this.n][this.m];
        generateMatrix();
    }

    public void setM(int m) {
        this.m = m;
        this.matrix = new int[this.n][this.m];
        generateMatrix();
    }


    public void setMatrix(int[][] matrix) throws MatrixLengthException {
        System.out.println("Размерность матрицы (" + this.m + "x" + matrix[0].length + ")");

        if (matrix.length == this.n) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length != this.m) {
                    throw new MatrixLengthException("Некорректная матрица");
                }
            }
        } else {
            throw new MatrixLengthException("Некорректная матрица");
        }

        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }


    public int[][] getMatrix() {
        return matrix;
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    public void generateMatrix() {
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++) {
                this.matrix[i][j] = (int) (Math.random() * 10);
            }
        }
    }
}
