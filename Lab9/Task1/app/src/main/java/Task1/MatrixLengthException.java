package Task1;

public class MatrixLengthException extends Exception{
    String message;

    public MatrixLengthException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
