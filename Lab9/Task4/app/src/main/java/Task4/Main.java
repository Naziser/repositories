package Task4;

public class Main {
    public static void main(String[] args) throws NotTriangleException {

        int[] sidesForFirst = new int[]{7,7,7};
        Triangle firstTriangle = new Triangle(sidesForFirst);

        int[] sidesForSecond = new int[]{1,2,3};
        Triangle secondTriangle = new Triangle(sidesForSecond);

        int[] sidesForThird = new int[]{1,1,2};
        Triangle thirdTriangle = new Triangle(sidesForThird);

        int[] sidesForPolygon = new int[]{1,2,3,4,5};
        Polygon polygon = new Polygon(sidesForPolygon);

        Text text1 = new Text("Я текст номер один!");
        Text text2 = new Text("Я текст номер два!");

        PublicInterface[] in = new PublicInterface[]{firstTriangle,secondTriangle,thirdTriangle,polygon,text1,text2};
        Operations operations = new Operations(in);
        operations.printAllObjects();
    }
}
