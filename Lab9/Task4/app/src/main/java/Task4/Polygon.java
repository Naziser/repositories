package Task4;

public class Polygon implements PublicInterface{
    protected int perimeter;
    protected int[] sides;

    Polygon(int[] sides){
        this.sides = sides;
        this.perimeter = polygonPerimeter();
        this.checkPolygonCorrect();
    }

    Polygon(){}

    public int polygonPerimeter(){
        int perimeter = 0;
        for (int i = 0; i < this.sides.length; i++) {
            perimeter += this.sides[i];
        }
        return perimeter;
    }

    public boolean checkPolygonCorrect(){
        int sum = this.perimeter;
        for (int i = 0; i < this.sides.length; i++){
            if (2 * this.sides[i] > sum){
              return false;
            }
        }
        return true;
    }

    public String output(){
        return "Я " + this.sides.length + "-угольник, периметр которого равен " + this.perimeter;
    }
}
