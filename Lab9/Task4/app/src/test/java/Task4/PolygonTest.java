package Task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {

  @Test void polygonPerimeterTest(){
    int[] a = new int[]{2,2,2,2,2};
    Polygon b = new Polygon(a);
    assertEquals(10,b.polygonPerimeter());
  }

  @Test void toStringPolygon(){
    int[] a = new int[]{2,2,2,2,2};
    Polygon b = new Polygon(a);
    assertEquals("Я 5-угольник, периметр которого равен 10", b.output());
  }

  @Test void checkTrueCorrect(){
    int[] a = new int[]{4,24,100};
    Polygon b = new Polygon(a);
    assertFalse(b.checkPolygonCorrect());
  }

  @Test void checkFalseCorrect(){
    int[] a = new int[]{1,2,3};
    Polygon b = new Polygon(a);
    assertTrue(b.checkPolygonCorrect());
  }
}
