package Task3;

public class ChiefAccountant extends Employee {

    public ChiefAccountant(String name, int salary){
        super(name,salary);
        this.setPosition("ChiefAccountant");
    }

    public String toString(){
        return "ChiefAccountant [chiefAccountantName = " + getName()
                + ", chiefAccountantSalary = " + getSalary()
                + ", chiefAccountantPosition = " + getPosition() + "]";
    }

    public String ChiefAccountant(){
        return "Я делаю всё, что и обычный бухгалтер, но мне платят больше)";
    }

    public boolean equals(Object o){
        super.equals(o);
        ChiefAccountant chAccountant = (ChiefAccountant) o;
        if (chAccountant.getName() == this.getName() && chAccountant.getSalary() == this.getSalary() && chAccountant.getPosition() == this.getPosition()){
            return true;
        }
        return false;
    }
}
