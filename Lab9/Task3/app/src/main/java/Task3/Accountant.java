package Task3;

public class Accountant extends Employee {

    public Accountant(String name, int salary){
        super(name,salary);
        this.setPosition("Accountant");
    }

    public String toString(){
        return "Accountant [accountantName = " + getName()
                + ", accountantSalary = " + getSalary()
                + ", accountantPosition = " + getPosition() + "]";
    }

    public String accountant(){
        return "Я занимаюсь проверкой финансовой информации, подготовкой документов и сдачей отчетности";
    }

    public boolean equals(Object o){
        super.equals(o);
        Accountant accountant = (Accountant) o;
        if (accountant.getName() == this.getName() && accountant.getSalary() == this.getSalary() && accountant.getPosition() == this.getPosition()){
            return true;
        }
        return false;
    }
}
