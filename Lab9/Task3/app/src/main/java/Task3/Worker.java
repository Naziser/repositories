package Task3;

public class Worker extends Employee {
    public Worker(String name, int salary){
        super(name,salary);
        this.setPosition("Worker");
    }

    public String toString(){
        return "Worker [workerName = " + getName()
                + ", workerSalary = " + getSalary()
                + ", workerPosition = " + getPosition() + "]";
    }

    public String worker(){
        return "Я рабочий...";
    }

    public boolean equals(Object o){
        super.equals(o);
        Worker worker = (Worker) o;
        if (worker.getName() == this.getName() && worker.getSalary() == this.getSalary() && worker.getPosition() == this.getPosition()){
            return true;
        }
        return false;
    }
}
