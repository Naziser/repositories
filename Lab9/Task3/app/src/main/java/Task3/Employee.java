package Task3;

public class Employee {
    private String name;
    private int salary;
    private String position;

    public Employee(String name, int salary){
        this.setName(name);
        this.setSalary(salary);
        this.setPosition("Employee");
    }

    public String toString(){
        return "Employee [employeeName = " + name
                + ", employeeSalary = " + salary
                + ", employeePosition = " + position + "]";
    }

    public boolean equals(Object o) {
      if (o == this){
        return true;
      }
      if (o == null){
        return false;
      }
      if (o.getClass() == this.getClass()){
          Employee employee = (Employee) o;
          if (employee.getName() == this.getName() && employee.getSalary() == this.getSalary() && employee.getPosition() == this.getPosition()){
              return true;
          }
      }
      return false;
    }

    public int hashCode() {
        return name.length() * 100 + salary + position.length() * 100;
    }

    public Object clone(){
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
    }



    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public int getSalary(){
        return this.salary;
    }

    public void setPosition(String position){
        this.position = position;
    }

    public String getPosition(){
        return this.position;
    }

}
