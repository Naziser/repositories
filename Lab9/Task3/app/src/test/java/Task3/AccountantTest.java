package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AccountantTest{
    @Test void accountant(){
        Accountant a = new Accountant("Oleg", 10000);
        assertEquals("Я занимаюсь проверкой финансовой информации, подготовкой документов и сдачей отчетности", a.accountant());
    }

    @Test void toStringTest(){
        Accountant a = new Accountant("Nikita", 20000);
        assertEquals("Accountant [accountantName = Nikita, accountantSalary = 20000, accountantPosition = Accountant]",a.toString());
    }

}
