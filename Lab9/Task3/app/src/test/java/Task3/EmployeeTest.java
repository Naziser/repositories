package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest{

  @Test void toStringTest(){
      Employee a = new Employee("Petr", 10000);
      assertEquals("Employee [employeeName = Petr, employeeSalary = 10000, employeePosition = Employee]", a.toString());
  }

  @Test void trueEqualsTest(){
      Employee a = new Employee("James", 20000);
      Employee b = new Employee("James", 20000);
      assertTrue(a.equals(b));
  }

  @Test void falseEqualsTest(){
      Employee a = new Employee("Danil", 10000);
      Employee b = new Employee("Nikita", 20000);
      assertFalse(a.equals(b));
  }

  @Test void hashCodeTest(){
      Employee a = new Employee("Magomed", 1000);
      assertEquals(2500, a.hashCode());
  }

  @Test void cloneTest(){
      Employee a = new Employee("Bill", 10000);
      assertNull(a.clone());
  }
}
