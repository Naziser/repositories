package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ChiefAccountantTest{

  @Test void chiefAccountant(){
      ChiefAccountant a = new ChiefAccountant("Oleg", 10000);
      assertEquals("Я делаю всё, что и обычный бухгалтер, но мне платят больше)", a.ChiefAccountant());
  }

  @Test void toStringTest(){
      ChiefAccountant a = new ChiefAccountant("Nikita", 20000);
      assertEquals("ChiefAccountant [chiefAccountantName = Nikita, chiefAccountantSalary = 20000, chiefAccountantPosition = ChiefAccountant]",a.toString());
  }
}
