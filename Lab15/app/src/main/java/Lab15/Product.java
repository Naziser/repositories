package Lab15;

import java.io.Serializable;

public class Product implements Serializable{

    private String title;
    private int productNumber;

    static final long serialVersionUID = 30275534472837494L;

    public Product(String title, int productNumber){
        this.title = title;
        this.productNumber = productNumber;
    }

    public String getTitle(){
        return this.title;
    }

    public int getProductNumber(){
        return this.productNumber;
    }

    public String toString(){
        return "Title : " + getTitle() + "\n" +
               "ProductNumber : " + getProductNumber();
    }
}
