package Lab15;

import java.util.Random;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;



public class Action {

    static Random random = new Random();

    final static int NUMBER_CHAR_A = 97;

    final static int COUNT_CHAR = 26;

    final static int COUNT_SIMBOL = 10;

    final static int MAX_INT_VALUES = 1000;

    final static int MAX_KEY_VALUES = 100;


    public static String generateString(){
        StringBuilder buffer = new StringBuilder(COUNT_SIMBOL);
        for (int i = 0; i < COUNT_SIMBOL; i++) {
            int randomLimitedInt = NUMBER_CHAR_A + (int)(random.nextFloat() * COUNT_CHAR);
            buffer.append((char) randomLimitedInt);
        }
        return new String(buffer);
    }

    public static Integer generateInteger(){
        Integer generateInt = random.nextInt(MAX_INT_VALUES);
        return generateInt;
    }

    public static Map<Integer,Product> generateMap(){
        Map<Integer,Product> map = new HashMap<Integer,Product>();
        for (int i = 0; i < MAX_KEY_VALUES; i++){
            map.put(i, new Product(generateString(),generateInteger()));
        }
        return map;
    }

    public static void writeFile(String fileName) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(generateMap());
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void readFile(String fileName) {

        Map<Integer, Product> map = new HashMap<Integer, Product>();

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileInputStream);
            map = (Map<Integer, Product>)in.readObject();
            in.close();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        Set<Map.Entry<Integer, Product>> set = map.entrySet();
        for (Map.Entry<Integer, Product> obj : set) {
            System.out.print(obj.getKey() + ": ");
            System.out.println(obj.getValue());
            System.out.println("\n");
        }
    }





}
