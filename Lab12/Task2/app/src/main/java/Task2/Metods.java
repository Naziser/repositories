package Task2;

import java.util.*;

public class Metods{

  public void hashMap(Maps map){
    final int SEC = 1000;
    double start = System.currentTimeMillis();
    System.out.println("HashMap:");
    map.time();
    map.sum(map.mass());
    double time = System.currentTimeMillis() - start;
    System.out.println("All: " + time/SEC + " sec");
    System.out.println("-------------------------------------------------");
  }


  public void treeMap(Maps map){
    final int SEC = 1000;
    double start1 = System.currentTimeMillis();
    System.out.println("TreeMap:");
    map.generateValues();
    map.sum(map.mass());
    double time1 = System.currentTimeMillis() - start1;
    System.out.println("All: " + time1/SEC + " sec");
    System.out.println("-------------------------------------------------");
  }


  public void linkedHashMap(Maps map){
    final int SEC = 1000;
    double start2 = System.currentTimeMillis();
    System.out.println("LinkedHashMap:");
    map.generateValues();
    map.sum(map.mass());
    double time2 = System.currentTimeMillis() - start2;
    System.out.println("All: " + time2/SEC + " sec");
    System.out.println("-------------------------------------------------");
  }


  public void hashtable(Maps map){
    final int SEC = 1000;
    double start3 = System.currentTimeMillis();
    System.out.println("Hashtable:");
    map.generateValues();
    map.sum(map.mass());
    double time3 = System.currentTimeMillis() - start3;
    System.out.println("All: " + time3/SEC + " sec");
    System.out.println("-------------------------------------------------");
  }
}
